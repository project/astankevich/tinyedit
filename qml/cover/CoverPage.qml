import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    CoverPlaceholder {
        text: (fileName ? fileName : "TinyEdit") + "\n" + (unsavedChanges ? qsTr("(modified)") : "")
        icon.source: "/usr/share/icons/hicolor/86x86/apps/harbour-tinyedit.png"
    }
    CoverActionList {
        enabled: pageStack.depth == 1
        CoverAction {
            iconSource: "image://theme/icon-cover-sync"
            onTriggered: pageStack.currentPage.save()
        }
    }
}
