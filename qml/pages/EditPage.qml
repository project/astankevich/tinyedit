import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.tinyedit 1.0

Page {
    id: page
    allowedOrientations: Orientation.All

    TextFile {
        id: file
    }

    SilicaFlickable {
        id: view
        anchors {
            fill: parent
            bottomMargin: errorPanel.margin
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("New")
                onClicked: clear()
            }
            MenuItem {
                text: qsTr("Save as")
                onClicked: saveAs()
            }
            MenuItem {
                text: qsTr("Open")
                onClicked: open()
            }
            MenuItem {
                text: qsTr("Save")
                onClicked: save()
            }
        }

        PushUpMenu {
            MenuItem {
                text: qsTr("Save")
                onClicked: save()
            }
            MenuItem {
                text: qsTr("Open")
                onClicked: open()
            }
            MenuItem {
                text: qsTr("Save as")
                onClicked: saveAs()
            }
            MenuItem {
                text: qsTr("New")
                onClicked: clear()
            }
        }

        contentHeight: area.height

        TextArea {
            id: area
            width: page.width
            selectionMode: TextEdit.SelectCharacters
            focus: true
            background: null
            onTextChanged: unsavedChanges = true
        }

        VerticalScrollDecorator { flickable: view }
    }

    DockedPanel {
        id: errorPanel
        width: parent.width
        height: Theme.itemSizeSmall + Theme.paddingSmall
        dock: Dock.Bottom

        Rectangle {
            anchors.fill: parent
            color: Theme.secondaryHighlightColor
            opacity: 0.3
        }

        Label {
            id: errorLabel
            anchors.centerIn: parent
            color: Theme.secondaryColor
        }

        Timer {
            id: errorTimer
            interval: 2000
            onTriggered: errorPanel.hide()
        }

        function showError(msg) {
            errorLabel.text = msg
            errorTimer.start()
            show()
        }
    }

    function clear() {
        file.path = ""
        area.text = ""
        fileName = ""
        unsavedChanges = false
    }

    function open() {
        pageStack.push("OpenPage.qml",
                       {"editor": page})
    }

    function save() {
        if (file.path != "") {
            doSave()
        } else {
            saveAs()
        }
    }

    function doSave() {
        if (file.save(area.text)) {
            unsavedChanges = false
        } else {
            errorPanel.showError(qsTr("Save failed"))
        }
    }

    function saveAs() {
        pageStack.push("SavePage.qml",
                       {"editor": page})
    }

    function openFile(path) {
        file.path = path
        area.text = file.load()
        fileName = file.fileName
        unsavedChanges = false
    }

    function saveFile(path) {
        file.path = path
        fileName = file.fileName
        doSave()
    }
}
