# The name of your app.
# NOTICE: name defined in TARGET has a corresponding QML filename.
#         If name defined in TARGET is changed, following needs to be
#         done to match new name:
#         - corresponding QML filename must be changed
#         - desktop icon filename must be changed
#         - desktop filename must be changed
#         - icon definition filename in desktop file must be changed
TARGET = harbour-tinyedit

CONFIG += sailfishapp

SOURCES += \
    src/tinyedit.cpp

OTHER_FILES += qml/harbour-tinyedit.qml \
    qml/cover/CoverPage.qml \
    rpm/harbour-tinyedit.spec \
    rpm/harbour-tinyedit.yaml \
    harbour-tinyedit.desktop \
    qml/pages/EditPage.qml \
    qml/pages/OpenPage.qml \
    qml/pages/SavePage.qml

HEADERS += \
    src/tinyedit.h

